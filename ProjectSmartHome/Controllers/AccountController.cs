﻿using ProjectSmartHome.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using ProjectSmartHome.Logic.Autorization;


namespace ProjectSmartHome.Controllers
{
    public class AccountController : Controller
    {
        HomeContext db = new HomeContext();

        // GET: Account
      
        public ActionResult Login()
        {
            return View();
        }      

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
{
            if (ModelState.IsValid)
            {
                User user = db.Users.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Email и/или Пароль указан неверно.");
                    return View(model);
                }
                else {
                    FormsAuthentication.SetAuthCookie(model.Login, true);
                    return RedirectToAction("Account", "Home");
                }
            }
           
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    var status = Autorization.CreateUser(model.Login, model.Email, model.Password);
                    if (status == Autorization.AutorizationEnum.DuplicateEmail)
                    {
                        ModelState.AddModelError("", "Данный Email уже используется");
                        return View(model);
                    }

                    else if (status == Autorization.AutorizationEnum.InvalidLogin)
                    {
                        ModelState.AddModelError("", "Пользователь с таким логином уже существует");
                        return View(model);
                    }

                    FormsAuthentication.SetAuthCookie(model.Login, true);
                    return RedirectToAction("Account", "Home");
                }
                catch(Exception) {
                    ModelState.AddModelError("", "Ошибка регистрации");
                } 
            }

            return View(model);
        }
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
       public ActionResult ForgotPassword(ForgotPasswordViewModel model)// отправки на сервер не происходит, происходит просто переход       
       {//происходит переход на другое действие
            if (ModelState.IsValid)
            {
                try
                {
                    var status = Autorization.ForgotPassword(model.Email);
                    if (status == Autorization.AutorizationEnum.InvalidEmail)
                    {
                        ModelState.AddModelError("", "Такой почты не существует");
                        return View(model);
                    }

                    return RedirectToAction("ResetPassword", "Account");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Ошибка при отправлении");
                }
            }

            return View(model);   
        }


        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            return View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var status = Autorization.ResetPassword(model.Email, model.Password);
                    if (status == Autorization.AutorizationEnum.InvalidEmail)
                    {
                        ModelState.AddModelError("", "Email указан неверно");
                        return View(model);
                    }

                    return RedirectToAction("ResetPasswordConfirmation", "Account");

                }
                catch (Exception) {
                    ModelState.AddModelError("", "Произошла ошибка при смене пароля");                  
                }
            }

            return View(model);
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }
    }
}
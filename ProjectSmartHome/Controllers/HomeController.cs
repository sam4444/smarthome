﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ProjectSmartHome.Models;
using static ProjectSmartHome.Models.ArduinoData;
using System.Data.Entity;
using System.Threading;
using ProjectSmartHome.Logic;

namespace ProjectSmartHome.Controllers
{
    public class HomeController : Controller
    {

       
        private  HomeContext db = new HomeContext();
        private int pageSize = 7;
        

        private void ConnectWithArduino(Device device)
        {
            GetCurrentPort().Write("connect");
            Thread.Sleep(4000);
            string code =ConvertData(GetCurrentPort().ReadLine());

            if (code == device.UniqueTitle)
            {
                GetConnectArduino().Start();
            }
        }
        private void DisconnectWithArduino(Thread connectArduino, Device device)
        {
            GetCurrentPort().Write("connect");
            Thread.Sleep(3000);
            string code = ConvertData(GetCurrentPort().ReadLine());

            if (code == device.UniqueTitle)
            {
                GetCurrentPort().Write("delete");
                Thread.Sleep(1000);
                connectArduino.Abort();
            }
        }

        public ActionResult Store(int? id)
        {
            if (id != null)
            {
                var commands =  db.Commands.Where(c=>c.DeviceId == id).OrderByDescending(c=>c.CommandTime)
                    .Include(v => v.Value)
                    .Include(t => t.Type)
                    .ToListAsync();
                 ViewBag.Id = id;
           
                 return PartialView(commands);

            }

            return RedirectToAction("NotFound1");
        }
        public ImageResult Image()
        {
            return new ImageResult(System.IO.File.OpenRead("~App_Data/image.jpg"), "image/gif");
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NotFound1()
        {
            return View();
        }
        public ActionResult NotDevice()
        {
            return View();
        }
        public ActionResult Account(int pageNum = 0)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = db.Users.First(u => u.Login == User.Identity.Name);
                Thread.Sleep(2000);

                ViewData["ItemCount"] =  db.Devices.Where(d=>d.UserId == user.Id).Count();
                ViewData["PageSize"] = pageSize;
                ViewData["PageNum"] = pageNum;

                Thread.Sleep(2000);

                var devices = db.Devices.Where(d=>d.UserId == user.Id).OrderBy(d=>d.Title).Include(u => u.User)
                    .Skip(pageSize * pageNum)
                    .Take(pageSize)
                    .ToList();

                Thread.Sleep(2000);

                if (user.RoleId == 1)
                {
                    return RedirectToAction("DbUser", "Home");
                }
                
                return View(devices);

            }
            else
                return RedirectToAction("Login", "Account");
        }
        [HttpGet]
        public ActionResult UserSearch(string name)
        {
            var allUsers = db.Users.Where(u => u.Login.Contains(name)).ToList();

            return PartialView(allUsers);

        }
        public ActionResult DbUser(int pageNum = 0)
        {
            var admin = db.Users.First(u => u.Login == User.Identity.Name);
            Thread.Sleep(2000);

            if (admin.RoleId == 1)
            {
                var users = db.Users
                    .Where(u=>u.RoleId!=1)
                    .OrderBy(u=>u.UserName)
                    .Include(r => r.Role)
                    .ToList();

                Thread.Sleep(2000);

                ViewData["PageNum"] = pageNum;
                ViewData["ItemCount"] = users.Count();
                ViewData["PageSize"] = pageSize;

                return View(users);
            }

            else
            {
                return RedirectToAction("Account", "Home");
            }
        }
        [HttpGet]
        public ActionResult DetailsUser(int? id)
        {
             if(id != null)
             {
                 User user = db.Users.Find(id);

                 if (user != null)
                 {
                     return PartialView(user);
                 }
             }

            return RedirectToAction("NotFound1");//
        }

        [HttpGet]
        public ActionResult EditUser(int? id)
        {
            if (id != null)
            {
                var user = db.Users.Find(id);
                if (user != null)
                {
                    SelectList role = new SelectList(db.Roles, "Id", "NameRole");
                    ViewBag.Roles = role;

                    return PartialView(user);
                }
            }

            return RedirectToAction("NotFound1");
        }

        [HttpPost]
        public ActionResult EditUser(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var status = ManageAdmin.UdateUser(user.Login, user.Password, user.UserName, user.RoleId);
                }

                catch { }
            }
            return RedirectToAction("DbUser");
        }
     
        public ActionResult ErrorMessageDevice()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddDevice(string name)
        {
            User user = null;
            try
            {
                if (name != null)
                {
                    user = db.Users.First(u => u.Login == name);
                }
                if (user != null)
                {
                    ViewBag.UserId = user.Id;
                    return PartialView();
                }
            }
            catch { }
            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDevice(Device device)
        {
            if (device.UserId == 0)
            {
                int userId = ManageUser.GetUserId(User.Identity.Name);
                if ( userId != -1)
                {
                    device.UserId = userId;
                }
                
            }
            if (ManageDevice.IsValid(device.Title, device.UniqueTitle, device.UserId))
            {
                try
                {
                    var state = ManageDevice.CreateDevice(device.Title, device.UniqueTitle, device.UserId);

                    if (state == ManageDevice.DeviceEnum.DuplicateCode)
                    {
                        ModelState.AddModelError("", "Устройство с таким уникальным кодом уже существует");
                        return View(device);                     
                    }

                    return RedirectToAction("Account","Home");
                }
                catch(Exception)
                {
                    ModelState.AddModelError("", "Введите название устройства");
                }
            }

            return View();                       
        }

        [HttpGet]
        public ActionResult EditDevice(int? id)
        {
            if (id != null)
            {
                Device device = db.Devices.Find(id);

                if (device != null)
                    return PartialView(device);
            }

            return RedirectToAction("NotFound1");

        }

        [HttpPost]
        public ActionResult EditDevice(Device device)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var status = ManageDevice.UpdateDevice(device.Title, device.UniqueTitle);

                    if (status == ManageDevice.DeviceEnum.NotFound)
                    {
                        ModelState.AddModelError("", "Не найдено устройство");
                        return View(device);
                    }

                    return RedirectToAction("Account", "Home");
                }
                catch(Exception)
                {
                    ModelState.AddModelError("", "Что-то пошло не так");
                }
            }

            return View(device);
            
        }

        [HttpGet]
        public ActionResult DeleteDevice(int? id)
        {
            if (id != null)
            {
                Device device = db.Devices.Find(id);
                
                if (device != null)
                    return PartialView(device);
            }

            return RedirectToAction("NotFound1");//      
        }

        [HttpPost, ActionName("DeleteDevice")]
        public ActionResult DeleteDeviceConfirmed(int? id)
        {
            try
            {
                if (id != null)
                {
                    Device device = db.Devices.Find(id);

                    if (device != null)
                    {
                        if (GetCurrentPort() != null)
                        {
                            DisconnectWithArduino(GetConnectArduino(), device);
                        }

                        var status = ManageDevice.RemoveDevice(id);

                        if (status == ManageDevice.DeviceEnum.NotFound)
                        {
                            ModelState.AddModelError("", "Устройстро не найдено");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("NotFound1");
                }
            }
            catch {}

            return RedirectToAction("Account");
        }

        [HttpGet]
        public ActionResult DetailsDevice(int? id)
        {
            if (id != null)
            {
                Device device = db.Devices.Find(id);

                if (device != null)
                {
                    return PartialView(device);
                }
            }

            return RedirectToAction("NotFound1");
        }

        [HttpGet]
        public ActionResult Manipulate(int? id)
        {
            if (id != null)
            {
                Device device =  db.Devices.Find(id);

                if (device != null)
                {
                    Command command = db.Commands.Create();

                    SelectList typeCommand = new SelectList(db.TypeCommands, "Id", "TittleCommand");
                    SelectList value = new SelectList(db.ValueTemperatures, "Id", "Value");

                    ViewBag.typeCommand = typeCommand;
                    ViewBag.value = value;

                    return PartialView(command);
                }
            }

            return RedirectToAction("NotFound1");
        }

        [HttpPost]
        public ActionResult Manipulate(Command command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (GetCurrentPort().IsOpen)
                    {
                        GetCurrentPort().Write(command.DeviceUniqueTitle + " " + command.TypeId.ToString());

                        var status = ManageCommand.CreateCommand(command.TypeId, command.ValueId, command.DeviceUniqueTitle, command.DeviceId);
                        if (status == ManageCommand.CommandEnum.NotFound)
                        {
                            ModelState.AddModelError("", "Не найдено устройство");
                        }
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Произошла ошибка");
                }
            }

            return RedirectToAction("Account");
        }

      
        [HttpGet]
        public ActionResult EditUserName()
        {
            var user = db.Users.First(u => u.Login == User.Identity.Name);
            ViewBag.UserId = user.Id;
            return View(user);
        }

        [HttpPost]
        public ActionResult EditUserName(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var status = ManageUser.UdateName(user.UserName, User.Identity.Name);
                    if (status == ManageUser.UserEnum.InvalidName)
                    {
                        ModelState.AddModelError("", "Введите имя пользователя");
                        return View(user);
                    }
                    else if (status == ManageUser.UserEnum.NotFound)
                    {
                        return RedirectToAction("NotFound1");
                    }
                }
                catch(Exception){   }
            }
            
            return RedirectToAction("Account");
        }

        [HttpGet]
        public  ActionResult ChangePassword()
        {
            return PartialView();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result =  db.Users.First(u => u.Login == User.Identity.Name);
            result.Password = model.NewPassword;

            db.SaveChanges();
            
            return RedirectToAction("Account");
        }       

        public ActionResult DeviceData(int? id)
        {
            if (id != null)
            {
                var deviceData =  db.OutputDatas.Where(d=>d.DeviceId == id).OrderByDescending(d=>d.Time).Include(d => d.Device).ToList();

                if (deviceData != null)
                {
                    return PartialView(deviceData);
                }
            }

            return RedirectToAction("NotFound1");
        }

        [HttpGet]
        public ActionResult DrawChart(int? id)
       {
            if (id == null)
            {
                return RedirectToAction("Account");
            }
            else
            {
                var Device =  db.Devices.Find(id);
                if (Device == null)
                {
                    return RedirectToAction("NotFound1");
                }
                else
                {
                    SelectList period = new SelectList(db.Periods, "Id", "Title");

                    ViewBag.Period = period;
                    ViewBag.Id = id;               
                    return View();
                }
            }
        }
        [HttpGet]
        public ActionResult Draw(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Account");
            }
            else
            {
                var outputData = db.OutputDatas.Where(o=>o.DeviceId == id)
                   // .Where(o=>o.Time >= DateTime.Now.AddHours(-1))
                    .ToList();
                if (outputData == null)
                {
                    return RedirectToAction("NotFound1");
                }
                else
                {
                    object[] valueTime;
                    var graphics = new List<object>();
                    
                    valueTime = new object[] { "Температура", "Время" };
                    graphics.Add(valueTime);
                   int i = 0;

                    while ( i < outputData.Count)
                    {
                        valueTime = new object[] { outputData[i].Time.ToString(), outputData[i].TemperatureRoom };
                        graphics.Add(valueTime);
                        i++;
                    }
                    return Json(graphics, JsonRequestBehavior.AllowGet);
                }
            }
        }
        
    }
}
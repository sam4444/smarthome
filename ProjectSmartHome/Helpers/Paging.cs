﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
namespace ProjectSmartHome.Helpers
{
    public static class Paging
    {
        public static MvcHtmlString PagingNavigator(this HtmlHelper helper, int pageNum,int itemCount,int pageSize)
        {
            StringBuilder sb = new StringBuilder();

            if (pageNum > 0)
            {
                sb.Append(helper.ActionLink("Предыдущая ", "Account", new { pageNum = pageNum - 1 }));
            }
            else
            {
                sb.Append(HttpUtility.HtmlEncode(" "));
            }

            sb.Append(" ");

            int pageCount = (int)(Math.Ceiling((double)itemCount / pageSize));
            if (pageNum < pageCount - 1)
            {
                sb.Append(helper.ActionLink(" Следующая", "Account", new { pageNum = pageNum + 1 }));
            }
            else
            {
                sb.Append(" ");
            }
            return MvcHtmlString.Create(sb.ToString());
        }
    }
}
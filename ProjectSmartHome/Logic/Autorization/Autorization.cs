﻿using ProjectSmartHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ProjectSmartHome.Logic.Autorization
{
    public static class Autorization
    {
        public static AutorizationEnum CreateUser(string firstName, string email, string password)
        {
            var result = AutorizationEnum.Success;
            using (var context = new HomeContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Email == email || x.Login == firstName);
                if (user == null)
                {
                    user = new User();
                    user.Email = email;
                    user.UserName = firstName;
                    user.Login = firstName;
                    user.RoleId = 2;
                    user.Password = password;
                    user.TimeRegistration = DateTime.Now;
                    context.Users.Add(user);
                    context.SaveChanges();
                }
                else if (user.Email == email)
                {
                    result = AutorizationEnum.DuplicateEmail;
                }
                else {
                    result = AutorizationEnum.InvalidLogin;
                }
            }
            return result;
        }

        public static AutorizationEnum CheckPassword(string email, string password)
        {
            var result = AutorizationEnum.Success;
            using (var context = new HomeContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Email == email);
                if (user == null)
                {
                    result = AutorizationEnum.InvalidEmail;
                }
                else
                {
                    if (user.Password != password)
                    {
                        result = AutorizationEnum.InvalidPassword;
                    }
                }
            }
            return result;
        }

        public static AutorizationEnum ChangePassword(string email, string password, string newPassword)
        {
            var result = AutorizationEnum.Success;
            using (var context = new HomeContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Email == email);
                if (user == null)
                {
                    result = AutorizationEnum.InvalidEmail;
                }
                else
                {
                    if (user.Password == password)
                    {
                        user.Password = newPassword;
                        context.SaveChanges();
                    }
                    else
                    {
                        result = AutorizationEnum.InvalidPassword;
                    }
                }
            }
            return result;
        }

        public static AutorizationEnum ResetPassword(string email, string newPassword)
        {
            var result = AutorizationEnum.Success;
            using (var context = new HomeContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Email == email);
                if (user == null)
                {
                    result = AutorizationEnum.InvalidEmail;
                }
                else
                {
                   user.Password = newPassword;
                   context.SaveChanges();    
                }
            }
            return result;
        }

        public static AutorizationEnum ForgotPassword(string email)
        {
            var result = AutorizationEnum.Success;
            using (var context = new HomeContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Email == email);
                if (user == null)
                {
                    result = AutorizationEnum.InvalidEmail;
                }
                
            }
            return result;
        }



        public enum AutorizationEnum
        {
            DuplicateEmail,
            InvalidEmail,
            InvalidPassword,
            InvalidLogin,
            Success
        }
    }
}
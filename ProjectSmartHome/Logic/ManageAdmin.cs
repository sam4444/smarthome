﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectSmartHome.Models;
using System.Web;
using System.Data.Entity.Migrations;

namespace ProjectSmartHome.Logic
{
    public static class ManageAdmin
    {
        public static AdminEnum UdateUser(string login,string password, string name, int roleId )
        {
            var result = AdminEnum.Success;
            using (var context = new HomeContext())
            {
                User user = context.Users.FirstOrDefault(u => u.Login == login);
                if (user != null)
                {
                    user.UserName = name;
                    user.Login = login;
                    user.Password = password;
                    user.RoleId = roleId;
                    context.Users.AddOrUpdate(user);
                    context.SaveChanges();
                }
                else if (name == null)
                {
                    result = AdminEnum.InvalidName;
                }
                else
                {
                    result = AdminEnum.NotFound;
                }
            }

            return result;
        }

        public enum AdminEnum
        {
            InvalidName,
            NotFound,
            Success            
        }
    }
}
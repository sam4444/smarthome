﻿using ProjectSmartHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectSmartHome.Logic
{
    public static class ManageCommand
    {
        public static CommandEnum CreateCommand(int? typeCommand, int? valueTemerature, string uniqueDeviceCode, int deviceId)
        {
            var result = CommandEnum.Success;
            using (var context = new HomeContext())
            {
                var device = context.Devices.Find(deviceId);
                if (device != null)
                {

                    var command = new Command
                    {
                        TypeId = typeCommand,
                        ValueId = valueTemerature,
                        DeviceUniqueTitle = uniqueDeviceCode,
                        DeviceId = deviceId,
                        CommandTime = DateTime.Now,
                    };

                    if (command.TypeId == 2)
                    {
                        command.ValueId = null;
                    }

                    context.Commands.Add(command);
                    context.SaveChanges();
                }
                else
                {
                    result = CommandEnum.NotFound;
                }
             }
                
            return result;
        }


        public enum CommandEnum
        {
            NotFound,
            Success
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectSmartHome.Models;
using System.Web;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

namespace ProjectSmartHome.Logic
{
    public static class ManageDevice
    {

        public static DeviceEnum CreateDevice(string title, string code, int userId)
        {
            var result = DeviceEnum.Success;
            using (var context = new HomeContext())
            {
                var device = context.Devices.FirstOrDefault(x => x.UniqueTitle == code);
                if (device == null)
                {
                    device = new Device();
                    device.Title = title;
                    device.UniqueTitle = code;
                    device.UserId  = userId;
                    device.TimeAddDevice = DateTime.Now;
                    context.Devices.Add(device);
                    context.SaveChanges();
                }
                else if (device.UniqueTitle == code)
                {
                    result = DeviceEnum.DuplicateCode;
                }
                else if(title == null)
                {
                    result = DeviceEnum.InvalidTitle;
                }
            }
            return result;
        }     

        public static DeviceEnum UpdateDevice(string title, string code)
        {
            var result = DeviceEnum.Success;

            using (var context = new HomeContext())
            {
                var device = context.Devices.FirstOrDefault(x => x.UniqueTitle == code);
                if (device != null)
                {
                    device.Title = title;
                    context.Devices.AddOrUpdate(device);
                    context.SaveChanges();
                }

                else if (device == null)
                {
                    result = DeviceEnum.NotFound;
                }
            }

            return result;
        }

        public static DeviceEnum RemoveDevice(int? id)
        {
            var result = DeviceEnum.Success;
            using (var context = new HomeContext())
            {
                var device = context.Devices.Find(id);
                if (device != null)
                {
                    context.Devices.Remove(device);
                    context.SaveChanges();
                }
                else {
                    result = DeviceEnum.NotFound;
                }
            }

            return result;
        }

        public static bool IsValid(string titte, string uniqueCode, int userId)
        {
            return titte != null && uniqueCode != null && userId >= 1;
        }
        public enum DeviceEnum
        {
            DuplicateCode,
            InvalidTitle,
            NotFound,
            Success
        }
    }
}
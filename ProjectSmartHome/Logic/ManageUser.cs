﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectSmartHome.Models;
using System.Data.Entity.Migrations;

namespace ProjectSmartHome.Logic
{
    public static class ManageUser
    {
        public static int GetUserId(string userName)
        {
            User user = null;
            using (var context = new HomeContext())
            {
                user = context.Users.FirstOrDefault(u => u.Login == userName);
                if(user != null)
                    return user.Id;
            }

            return -1;            
        }

        public static UserEnum UdateName(string name,string login)
        {
            var result = UserEnum.Success;
            using (var context = new HomeContext())
            {
                User user = context.Users.FirstOrDefault(u => u.Login == login);
                if (user != null)
                {
                    user.UserName = name;
                    context.Users.AddOrUpdate(user);
                    context.SaveChanges();
                }
                else if (name == null)
                {
                    result = UserEnum.InvalidName;
                }
                else
                {
                    result = UserEnum.NotFound;
                }
            }

            return result;
        } 

        public enum UserEnum{
            InvalidName,
            NotFound,
            Success
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Web;

namespace ProjectSmartHome.Models
{
    public static class ArduinoData
    {
        static SerialPort _currentPort;
        static OutputData outData = new OutputData();     
        static Thread _connectArduino;

        public static SerialPort GetCurrentPort()
        {
            return _currentPort;
        }

        public static OutputData GetOutPut()
        {
            return outData;
        }

        public static Thread GetConnectArduino()
        {
            return _connectArduino;
        }
        public static void ArduinoClose()
        {
            _currentPort.Close();
        }

        public static void ArduinoOpen()
        {
            bool ArduinoPortFound = false;

            try
            {
                string[] ports = SerialPort.GetPortNames();
                foreach (string port in ports)
                {
                    _currentPort = new SerialPort(port, 9600);
                    if (port != null)
                    {
                        ArduinoPortFound = true;
                        break;
                    }
                    else
                    {
                        ArduinoPortFound = false;
                    }
                }
            }
            catch { }

            if (ArduinoPortFound == false) return;
            System.Threading.Thread.Sleep(500); // немного подождем 

            _currentPort.BaudRate = 9600;
            _currentPort.DtrEnable = true;
            _currentPort.ReadTimeout = 1000;
            try
            {
                _currentPort.Open();
                Thread.Sleep(2000);

                _currentPort.Write("connect");
                Thread.Sleep(6000);
                string uniqueCode = ConvertData(_currentPort.ReadLine());


                _connectArduino = new Thread(new ThreadStart(DataFromArduino));
                using (var db = new HomeContext())
                {

                    if (db.Devices.First(d => d.UniqueTitle == uniqueCode).Id != 0)
                    {
                        _connectArduino.Start();
                    }
                }
            }
            catch { }

        }

        private static float ReadData(string data)
        {
            float temerature;
            char[] arr = data.ToCharArray();
            string answer = "";

            foreach (char ch in arr)
            {
                if (Char.IsDigit(ch))
                {
                    answer += ch;
                }
                else if (ch == '.')
                {
                    answer += ',';

                }

                else
                    break;
            }
            if (answer != "")
            {
                temerature = float.Parse(answer);
            }
            else
                temerature = -1.0F;

            return temerature;
        }

        public static string ConvertData(string data)
        {
            char[] arr = data.ToCharArray();

            int count = 0;
            data = "";
            while (arr[count] != '\r')
            {
                data += arr[count];
                count++;
            }

            return data;
        }

        public static void DataFromArduino()
        {

            int count = 0;
            _currentPort.Write("connect");
            Thread.Sleep(4000);
            try
            {
                string uniqueCode = ConvertData(_currentPort.ReadLine());

                using (var db = new HomeContext())
                {

                    if (db.Devices.Count() > 0)
                    {
                        outData.DeviceId = db.Devices.First(d => d.UniqueTitle == uniqueCode).Id;

                        if (outData.DeviceId != 0)
                        {
                            _currentPort.Write(uniqueCode);

                            while (_currentPort.IsOpen)
                            {
                                Thread.Sleep(1000);
                                count++;
                                if (count % 15 == 0)//отправляет данные каждые 15 секунд
                                {
                                    outData.TemperatureRoom = ReadData(_currentPort.ReadExisting());
                                    if (outData.TemperatureRoom <= 1)
                                        continue;

                                    outData.Time = DateTime.Now;

                                    db.OutputDatas.Add(outData);
                                    db.SaveChanges();

                                    count = 0;
                                }

                            }
                        }
                    }
                }

            }
            catch { }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectSmartHome.Models
{
    public class Command
    {
        public int Id { get; set; }     
        public int? TypeId { get; set; }
        public int? ValueId { get; set; }
        public DateTime? CommandTime { get; set; }
        public int DeviceId { get; set; }
        public string DeviceUniqueTitle { get; set; }
        public Device Device { get; set; }

        public TypeCommand Type { get; set; }
        public ValueTemperature Value { get; set; }

    }
}
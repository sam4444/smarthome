﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectSmartHome.Models;
using System.ComponentModel.DataAnnotations;

namespace ProjectSmartHome.Models
{
    public class Device
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Уникальный код")]
        public string UniqueTitle { get; set; }
        public int UserId { get; set; }
        public DateTime? TimeAddDevice { get; set; }

        public User User { get; set; }

        public IEnumerable<Command> Commands { get; set; }
        public IEnumerable<OutputData> OutputDatas { get; set; }

        public Device()
        {
            Commands = new List<Command>();
            OutputDatas = new List<OutputData>();
        }

    }
}
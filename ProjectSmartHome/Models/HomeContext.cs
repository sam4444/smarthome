﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ProjectSmartHome.Models
{
    public class HomeContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Command> Commands { get; set; }
        public DbSet<TypeCommand> TypeCommands { get; set; }
        public DbSet<ValueTemperature> ValueTemperatures { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<OutputData> OutputDatas { get; set; }    
        public DbSet<Period> Periods { get; set; }
    }
    
    public class Repository
        {
            public static IQueryable<TEntity> Select<TEntity>()
                where TEntity : class
            {
                HomeContext context = new HomeContext();

                // Здесь мы можем указывать различные настройки контекста,
                // например выводить в отладчик сгенерированный SQL-код
                //context.Database.Log =
                //    (s => System.Diagnostics.Debug.WriteLine(s));

                // Загрузка данных с помощью универсального метода Set
                return context.Set<TEntity>();
            }
        }
    
}
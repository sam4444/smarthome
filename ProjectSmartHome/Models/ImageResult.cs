﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace ProjectSmartHome.Models
{
    public class ImageResult:ActionResult
    {
        private static Stream _ImageStream;

        private static string _ContentType = "";

        public static Stream ImageStream
        {
            get { return _ImageStream; }
            set { _ImageStream = value; }
        }

        public static string ContentType
        {
            get { return _ContentType; }
            set { _ContentType = value; }
        }

        public ImageResult(Stream imageStream, string contentType)
        {
            if (imageStream == null) throw new ArgumentNullException("imageStream");
            if (String.IsNullOrEmpty(contentType)) throw new ArgumentNullException("contentType");
            _ImageStream = imageStream;
            _ContentType = contentType;
        }
        
        public  override void  ExecuteResult(ControllerContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            HttpResponseBase Response = context.HttpContext.Response;
            Response.ContentType = _ContentType;
            byte[] buffer = new byte[1024];
            do
            {
                int read = _ImageStream.Read(buffer, 0, buffer.Length);
                if (read == 0) { break; }
                Response.OutputStream.Write(buffer, 0, read);
            } while (true);
            Response.End();
        }
    }
}
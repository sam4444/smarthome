﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectSmartHome.Models
{
    public  class OutputData
    {
        [Key]
        public int Id { get; set; }
        public float? TemperatureRoom { get; set; }
        public float? HumidityRoom { get; set; }
        public float? PowerFull { get; set; }
        public DateTime? Time { get; set; }
        public int DeviceId { get; set; }
        public Device Device { get; set; }






    }
}
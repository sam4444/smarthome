﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectSmartHome.Models
{
    public class Period
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
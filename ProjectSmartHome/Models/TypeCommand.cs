﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ProjectSmartHome.Models
{
    public class TypeCommand
    {
        [Key]
        public int Id { get; set; }
        public string TittleCommand { get; set; }
        public IEnumerable<TypeCommand> TypeCommands { get; set; }

        public TypeCommand()
        {
            TypeCommands = new List<TypeCommand>();
        }
    }
}
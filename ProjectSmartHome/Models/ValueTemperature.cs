﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ProjectSmartHome.Models
{
    public class ValueTemperature
    {
        [Key]
        public int Id { get; set; }
        public float Value{ get; set; }
        public IEnumerable<ValueTemperature> ValueTemperatures { get; set; }
        public ValueTemperature()
        {
            ValueTemperatures = new List<ValueTemperature>();
        }

    }
}
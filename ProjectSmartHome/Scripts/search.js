﻿$(document).ready(function () {
	$('#submit').click(function (e) {
		e.preventDefault();
		var name = $('#search').val();
		name = encodeURIComponent(name);
		if (name != "") {
			$("#main").hide();
			$('#results').load('@Url.Action("UserSearch", "Home")?name=' + name);
			$("#results").show();
		}
		else
		{
			$("#results").hide();
			$("#main").show();
		}
	});
});
